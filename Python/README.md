# Python support

To build the python TEOBResumS module run

```
python TEOBResumSWrap_setup.py build_ext --inplace
```

or type `make`

Other contents of this folder are

 * `CWrap/` : Tests for the python wrapper
 * `Examples/` : Examples to run `TEOBResumS` through the python interface
 * `Utils/` : Various scripts to help running the `TEOBResumS` executable with the parfiles
 
 

