# References

 * Nagar et al (2018) [Time-domain effective-one-body gravitational waveforms for coalescing compact binaries with nonprecessing spins, tides and self-spin effects](https://inspirehep.net/record/1676430)

**please cite the above reference** when using the code.

Other key references, please cite the ones relevant for the methods/functionalities you use

 * Nagar, Bonino (2021) [All in one: effective one body multipolar waveform model for spin-aligned, quasi-circular, eccentric, hyperbolic black hole binaries](https://inspirehep.net/literature/1842171)
 * Gamba, Bernuzzi, Nagar (2020) [Fast, faithful, frequency-domain effective-one-body waveforms for compact binary coalescences](https://inspirehep.net/literature/1834356) SPA 
 * Nagar, Rettegno, Gamba, Bernuzzi (2020) [Effective-one-body waveforms from dynamical captures in black hole binaries](https://inspirehep.net/literature/1819910) Dynamical capture
 * Akcay, Gamba, Bernuzzi (2020) [A hybrid post-Newtonian -- effective-one-body scheme for spin-precessing compact-binary waveforms](https://inspirehep.net/literature/1795896) Precession
 * Chiaramello, Nagar (2020) [Faithful analytical effective-one-body waveform model for spin-aligned, moderately eccentric, coalescing black hole binaries](https://inspirehep.net/literature/1778128) Eccentricity
 * Nagar, Riemenschneider, Pratten, Rettegno, Messina (2020) [A multipolar effective one body waveform model for spin-aligned black hole binaries](https://inspirehep.net/literature/1777194) Higher multipoles
 * Rettegno, Martinetti, Nagar, Bini, Riemenschneider (2020) [Comparing Effective One Body Hamiltonians for spin-aligned coalescing binaries](https://inspirehep.net/literature/1766919) TEOBResumS vs SEOBNR
 * Nagar, Messina, Rettegno, Bini, Damour, Geralico, Akcay, Bernuzzi (2018) [Nonlinear-in-spin effects in effective-one-body waveform models of spin-aligned, inspiralling, neutron star binaries](https://inspirehep.net/record/1710050)
 * Akcay, Bernuzzi, Messina, Nagar, Ortiz, Rettegno (2018) [Effective-one-body multipolar waveform for tidally interacting binary neutron stars up to merger](http://inspirehep.net/record/1707624) 
 * Nagar, Rettegno (2018) [Efficient effective one body time-domain gravitational waveforms](https://inspirehep.net/literature/1672501) PA approximation
 * Nagar, Riemenschneider, Pratten (2017) [Impact of Numerical Relativity information on effective-one-body waveform models](http://inspirehep.net/record/1518392)
 * Del Pozzo, Nagar (2016) [Analytic family of post-merger template waveforms](http://inspirehep.net/record/1469053)
 * Nagar, Damour, Reisswig, Pollney (2015) [Energetics and phasing of nonprecessing spinning coalescing black hole binaries](http://inspirehep.net/record/1380155) NRAR comparison, BBH spin
 * Bernuzzi, Nagar, Dietrich, Damour (2014) [Modeling the Dynamics of Tidally Interacting Binary Neutron Stars up to the Merger](http://inspirehep.net/record/1334334) TEOBResum
 * Bini, Damour (2014) [Gravitational self-force corrections to two-body tidal interactions and the effective one-body formalism](http://inspirehep.net/record/1318823)
 * Damour, Nagar (2014) [New effective-one-body description of coalescing nonprecessing spinning black-hole binaries](http://inspirehep.net/record/1303216) New formalism for spin with centrifugal radius
 * Damour, Nagar (2014) [A new analytic representation of the ringdown waveform of coalescing spinning black hole binaries](http://inspirehep.net/record/1298802)
 * Bini, Damour, Faye (2012) [Effective action approach to higher-order relativistic tidal interactions in binary systems and their effective one body description](http://inspirehep.net/record/1089309)
 * Damour, Nagar, Bernuzzi (2012) [Improved effective-one-body description of coalescing nonspinning black-hole binaries and its numerical-relativity completion](http://inspirehep.net/record/1207869)
 * Bernuzzi, Nagar, Thierfelder, Bruegmann (2012) [Tidal effects in binary neutron star coalescence](http://inspirehep.net/record/1114723) NRAR comparison, NNLO tidal model
 * Damour, Nagar (2009) [Effective One Body description of tidal effects in inspiralling compact binaries](http://inspirehep.net/record/838179) EOB formalism for tides
 * Damour, Nagar (2009) [Relativistic tidal properties of neutron stars](http://inspirehep.net/record/821786) Formalism relativistic Love numbers
 * Damour, Nagar (2009) [An Improved analytical description of inspiralling and coalescing black-hole binaries](http://inspirehep.net/record/812296)
 * Damour, Iyer, Nagar (2008) [Improved resummation of post-Newtonian multipolar waveforms from circularized compact binaries](http://inspirehep.net/record/802497) Resummed EOB waveform